require('dotenv').config();
const express = require('express');
const http = require('http');
const httpProxy = require('http-proxy');
const bodyParser = require('body-parser');
const jsonParser = bodyParser.json();

const cors = require('cors');
const Authentication = require('./lib/authentication');

const app = express();
const appServer = http.createServer(app);
const proxy = httpProxy.createProxyServer();

const port = process.env.PORT || 8080;

const userServiceUrl = process.env.USER_SERVICE;
const catalogServiceUrl = process.env.CATALOG_SERVICE;
const authServiceUrl = process.env.AUTH_SERVICE;

if (!userServiceUrl || !catalogServiceUrl || !authServiceUrl) {
  console.error('failed to find required endpoint targets');
  process.exit(1);
}

const authentication = Authentication({ authServiceUrl, userServiceUrl });

const services = [
  { path: [ '/api/user', '/api/user/*' ], target: userServiceUrl },
  { path: [ '/api/training', '/api/training/*' ], target: catalogServiceUrl }
];

console.log('User service endpoint: ', userServiceUrl);
console.log('Catalog service endpoint: ', catalogServiceUrl);
console.log('Auth service endpoint: ', authServiceUrl);

proxy.on('error', (err, req, res) => {
  if (err) {
    console.log('proxy error', err);
  }
});

// CORS Configuration
app.use(cors({
  origin: '*',
  methods: 'GET, POST, PUT, DELETE, OPTIONS, HEAD',
  credentials: true,
  allowedHeaders: 'Content-Type'
}));

app.use((req, res, next) => {
  console.log(`Request for ${req.path} received at ${Date.now()}`);
  next();
});

app.post('/api/auth/register', jsonParser, authentication.register);
app.post('/api/auth/login', jsonParser, authentication.login);
app.post('/api/auth/logout', jsonParser, authentication.logout);

services.forEach(service => {
  app.all(service.path, authentication.verifyToken, (req, res) => {
    proxy.web(req, res, { target: service.target });
  });
});

// 404 response for all other requests
app.all('*', (req, res) => {
  res.status(404).send('Route not found');
});

app.use((err, req, res) => {
  console.error('Error with request: ', err);
  res.status(err.status || 500);
  res.render('error', { message: err.message, error: {} });
});

appServer.listen(port, () => console.log(`Gateway listening on ${port}`));
